package com.cinnoca.abirechner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.lang.NumberFormatException
import kotlin.math.round

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // set onChangeListeners to always call updateABI
        RRNumberArmRechts.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateABI()
            }
        })

        RRNumberArmLinks.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateABI()
            }
        })

        RRNumberADPRechts.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateABI()
            }
        })

        RRNumberADPLinks.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateABI()
            }
        })

        RRNumberATPRechts.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateABI()
            }
        })

        RRNumberATPLinks.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateABI()
            }
        })
    }

    private fun updateABI(){
        var r1 = 0
        var l1 = 0
        var r2 = 0
        var l2 = 0
        var r3 = 0
        var l3 = 0

        // get RR input values
        try {
            // Cast everything to int
            r1 = RRNumberArmRechts.text.toString().toInt()
        }catch(e: NumberFormatException) {
        }


        try {
            // Cast everything to int
            l1 = RRNumberArmLinks.text.toString().toInt()
        }catch(e: NumberFormatException) {
        }

        try {
            // Cast everything to int
            r2 = RRNumberADPRechts.text.toString().toInt()
        }catch(e: NumberFormatException) {
        }

        try {
            // Cast everything to int
            l2 = RRNumberADPLinks.text.toString().toInt()
        }catch(e: NumberFormatException) {
        }

        try {
            // Cast everything to int
            r3 = RRNumberATPRechts.text.toString().toInt()
        }catch(e: NumberFormatException) {
        }

        try {
            // Cast everything to int
            l3 = RRNumberATPLinks.text.toString().toInt()
        }catch(e: NumberFormatException) {
        }

        //RECHTS
        // Try to calculate the ADP
        try {
            val calcs: Double = calcABI(r1,l1,r2)
            textABIAdpRechts.text = calcs.toString()
        }catch(e: Exception) {

        }// Try to calculate the ATP
        try {
            val calcs: Double = calcABI(r1,l1,r3)
            textABIAtpRechts.text = calcs.toString()
        }catch(e: Exception) {
        }

        //LINKS
        // Try to calculate the ADP
        try {
            val calcs: Double = calcABI(r1,l1,l2)
            textABIAdpLinks.text = calcs.toString()
        }catch(e: Exception) {

        }// Try to calculate the ATP
        try {
            val calcs: Double = calcABI(r1,l1,l3)
            textABIAtpLinks.text = calcs.toString()
        }catch(e: Exception) {
        }
    }

    private fun calcABI(a: Int, b: Int, c: Int): Double {
        return (c/((a+b)*0.5)).round(2)
    }

    //rounding to two decimals
    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        repeat(decimals) { multiplier *= 10 }
        return round(this * multiplier) / multiplier
    }
}